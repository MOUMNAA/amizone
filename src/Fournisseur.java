import java.util.ArrayList;

/**
 * Cette classe modelise un Fournisseur de site.
 *
 * @author
 * @version Automne 2020
 */
public class Fournisseur extends Utilisateur {


    //-----------------------------------
    // ATTRIBUTS ET CONSTANTES DE CLASSE
    //-----------------------------------

    //messages d'erreurs
    public static final String MSG_ERR_FOURNISSEUR_EVALUER_CONSOMMATEUR = "Erreur, " +
        "ce fournisseur ne peut pas evaluer ce consommateur.";
    public static final String MSG_ERR_PRIX_INVALIDE = "Erreur, prix invalide.";
    public static final String MSG_ERR_PRODUIT_EXISTE_DEJA = "Erreur, ce produit " +
        "ne peut etre ajoute car il existe deja.";
    public static final String MSG_ERR_PRODUIT_NON_VENDU_PAR_FOURNISSEUR = "Erreur, " +
        "ce produit n'est pas vendu par ce fournisseur.";


    //-----------------------------------
    // ATTRIBUTS D'INSTANCE
    //-----------------------------------
    private ArrayList<Produit> produits;  // La liste des produits vendus par ce fournisseur

    //-------------------
    // CONSTRUCTEURS
    //-------------------

    /**
     * Construit un fournisseur avec le pseudo,le motPasse et le courriel.
     *
     * @param pseudo   Le pseudonyme de fournisseur supposé valide (non null, non vides,
     *                 correctement formés, etc.)
     * @param motPasse Le mot de passe de fournisseur supposé valide (non null, non vides,
     *                 *                 * correctement formés, etc.)
     * @param courriel Le courriel de fournisseur supposé valide (non null, non vides,
     *                 *                 *      * correctement formés, etc.)
     */
    public Fournisseur(String pseudo, String motPasse, String courriel) {
        super(pseudo, motPasse, courriel);
        this.produits = new ArrayList<>();
    }

    /**
     * Constructeur de copie.
     *
     * @param fournisseur le fournisseur a copier.
     */
    public Fournisseur(Fournisseur fournisseur) {
        super(fournisseur);
        this.produits = (ArrayList<Produit>) fournisseur.produits.clone();
    }


    //--------------------------------------
    //  REDÉFINITIONS DES MÉTHODES D’INSTANCE ABSTRAITES DE LA SUPERCLASSE
    //--------------------------------------

    /**
     * Redefinition de la methode compilerProfil
     * retourne une liste de chaines de caractères représentant
     * le profil de ce fournisseur
     *
     * @return une liste de chaines de caractères représentant le profil de ce fournisseur
     */
    @Override
    public ArrayList<String> compilerProfil() {

        ArrayList<String> categories = new ArrayList<>();
        for (Produit produit : produits) {
            if (!categories.contains(produit.getCategorie()) && (produit.getQuantite() > 0)) {
                categories.add(produit.getCategorie());
            }
        }
        return categories;
    }


    /**
     * cette méthode test si ce consommateur a achete un produit de fournisseur en parametre
     *
     * @param consommateur le consommateur a verifier
     * @return true si le consommateur passé en parametre a achete un produit de ce fournisseur, false sinon
     */
    private Boolean isConsommateurAchterProduit(Utilisateur consommateur) {

        for (Produit achat : ((Consommateur) consommateur).getAchats()) {
            if (this.getId() == achat.getIdFournisseur()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Cette méthode permet à ce fournisseur d’évaluer un consommateur (celui reçu en paramètre).
     *
     * @param consommateur Le consommateur à évaluer par ce fournisseur
     * @param eval         L’évaluation donnée par ce fournisseur au consommateur reçu en
     *                     paramètre (entre 1 et 5 inclusivement)
     * @throws NullPointerException si le consommateur passé en paramètre est null.
     * @throws ClassCastException   si le paramètre consommateur n’est pas de type Consommateur.
     * @throws Exception            si le consommateur passé en paramètre n’a jamais acheté de produit(s) de ce fournisseur
     *                              ou si l’évaluation passée en paramètre (eval) est invalide.
     */
    @Override
    public void evaluer(Utilisateur consommateur, int eval) throws Exception {
        if (consommateur == null) {
            throw new NullPointerException();
        } else if (!(consommateur instanceof Consommateur)) {
            throw new ClassCastException();
        } else if (!isConsommateurAchterProduit(consommateur)) {
            throw new Exception(MSG_ERR_FOURNISSEUR_EVALUER_CONSOMMATEUR);
        } else {
            consommateur.ajouterEvaluation(eval);
        }
    }

    //--------------------------------------
    // METHODES D'INSTANCE PUBLIQUES
    //--------------------------------------

    //----------------------
    // GETTERS ET SETTERS
    //----------------------
    /**
     * cette methode retourne la liste des produits vendus
     *
     * @return la liste des produits vendus
     */
    public ArrayList<Produit> getProduits() {
        return produits;
    }

    /**
     * cette methode verifie si le produit existe au liste des produits de ce fournisseur
     *
     * @param nvProduit le produit a verifier
     * @return true si le produit existe au liste des produits de ce fournisseur,false sinon
     */
    private Boolean existeDeja(Produit nvProduit) {
        for (Produit produit : produits) {
            if (produit.equals(nvProduit)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Cette méthode permet d’ajouter un nouveau produit à vendre dans la liste des produits de ce fournisseur, si
     * celui-ci n’y est pas déjà.
     *
     * @param produit     Le produit à ajouter à la liste des produits vendus par ce fournisseur
     * @param qteEnStock  La quantité en stock initiale du produit ajouté
     * @param prixDeVente Le prix de vente du produit ajouté
     * @throws ExceptionProduitInvalide si le produit donné en paramètre est null.
     * @throws Exception                si la quantité en stock donnée est plus petite ou égale à 0
     *                                  ou si le prix de vente donné est plus petit ou égal à 0 (lors de l'ajout d'un produit à vendre, on doit lui
     *                                  assigner un prix valide (> 0) )
     *                                  ou si le produit donné en paramètre existe déjà dans la liste des produits vendus par ce fournisseur.
     */
    public void ajouterNouveauProduit(Produit produit, int qteEnStock, double prixDeVente) throws Exception {
        if (produit == null) {
            throw new ExceptionProduitInvalide();
        } else if (qteEnStock <= 0) {
            throw new Exception(Consommateur.MSG_ERR_QUANTITE_INVALIDE);
        } else if (prixDeVente <= 0) {
            throw new Exception(MSG_ERR_PRIX_INVALIDE);
        } else if (existeDeja(produit)) {
            throw new Exception(MSG_ERR_PRODUIT_EXISTE_DEJA);
        } else {
            Produit copieProduite = new Produit(produit);
            copieProduite.setIdFournisseur(this.getId());
            copieProduite.setQuantite(qteEnStock);
            copieProduite.setPrix(prixDeVente);
            produits.add(copieProduite);
        }
    }

    /**
     * Cette méthode recherche, dans la liste des produits vendus par ce fournisseur, le produit ayant le code donné
     *
     * @param code Le code du produit qu’on veut obtenir
     * @return dans la liste des produits vendus par ce fournisseur, le produit ayant le code donné
     */
    public Produit obtenirProduit(int code) {
        for (Produit produit : produits) {
            if (produit.getCode() == code) {
                return produit;
            }
        }
        return null;
    }

    /**
     * Cette méthode permet de diminuer la quantité en stock d’un produit dont on a vendu une certaine quantité
     *
     * @param codeProduit Le code du produit dont on veut diminuer la quantité en stock
     * @param qteVendue   La quantité vendue qu’on veut soustraire à la quantité en stock du produit
     *                    donné
     * @throws Exception si le code du produit donné ne correspond à aucun des produits vendus par ce fournisseur
     *                   ou si le code du produit donné ne correspond à aucun des produits vendus par ce fournisseur
     */
    public void vendre(int codeProduit, int qteVendue) throws Exception {
        Produit produit = obtenirProduit(codeProduit);
        if (!existeDeja(produit)) {
            throw new Exception(MSG_ERR_PRODUIT_NON_VENDU_PAR_FOURNISSEUR);
        } else if (qteVendue <= 0 || produit.getQuantite() < qteVendue) {
            throw new Exception(Consommateur.MSG_ERR_QUANTITE_INVALIDE);
        } else {
            produit.diminuerQuantite(qteVendue);
        }
    }

    /**
     * Retourne une representation sous forme de chaine de caracteres de ce
     * Fournisseur.
     *
     * @return une representation sous forme de chaine de caracteres de ce
     * Fournisseur.
     */
    public String toString() {
        return super.toString() + " - " + produits.size();
    }
}
