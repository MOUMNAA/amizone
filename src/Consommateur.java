import java.util.ArrayList;

/**
 * Cette classe modelise un consommateur de site.
 *
 * @author
 * @version Automne 2020
 */
public class Consommateur extends Utilisateur {

    //-----------------------------------
    // ATTRIBUTS ET CONSTANTES DE CLASSE
    //-----------------------------------

    //messages d'erreurs
    public static final String MSG_ERR_CONSOMMATEUR_EVALUER_FOURNISSEUR = "Erreur," +
        " ce consommateur ne peut pas evaluer ce fournisseur.";
    public static final String MSG_ERR_FOURNISSEUR_NON_TROUVE = "Erreur, ce produit " +
        "n'est vendu par aucun fournisseur.";
    public static final String MSG_ERR_QUANTITE_INVALIDE = "Erreur, quantite invalide.";


    //-----------------------------------
    // ATTRIBUTS D'INSTANCE
    //-----------------------------------

    private ArrayList<Produit> achats;  // La liste des produits achetés par ce consommateur

    //-------------------
    // CONSTRUCTEURS
    //-------------------

    /**
     * Construit un consommateur avec le pseudo,le motPasse et le courriel.
     *
     * @param pseudo   Le pseudonyme de consommateur supposé valide (non null, non vides,
     *                 correctement formés, etc.)
     * @param motPasse Le mot de passe de consommateur supposé valide (non null, non vides,
     *                 *                 * correctement formés, etc.)
     * @param courriel Le courriel de consommateur supposé valide (non null, non vides,
     *                 *                 *      * correctement formés, etc.)
     */
    public Consommateur(String pseudo, String motPasse, String courriel) {
        super(pseudo, motPasse, courriel);
        this.achats = new ArrayList<>();
    }

    /**
     * Constructeur de copie.
     *
     * @param consommateur le consommateur a copier.
     */
    public Consommateur(Consommateur consommateur) {
        super(consommateur);
        this.achats = (ArrayList<Produit>) consommateur.achats.clone();
    }

    //--------------------------------------
    //  REDÉFINITIONS DES MÉTHODES D’INSTANCE ABSTRAITES DE LA SUPERCLASSE
    //--------------------------------------


    /**
     * Redefinition de la methode compilerProfil
     * retourne une liste de chaines de caractères représentant
     * le profil de ce consommateur
     *
     * @return une liste de chaines de caractères représentant le profil de ce consommateur
     */
    @Override
    public ArrayList<String> compilerProfil() {

        ArrayList<String> categories = new ArrayList<>();
        for (Produit achat : achats) {
            if (!categories.contains(achat.getCategorie())) {
                categories.add(achat.getCategorie());
            }
        }
        return categories;
    }

    /**
     * cette méthode test si ce consommateur a achete un produit de fournisseur en parametre
     *
     * @param fournisseur le fournisseur a verifier
     * @return true si ce consommateur  a achete un produit de fournisseur en parametre, false sinon
     */
    private Boolean isConsommateurAchterProduit(Utilisateur fournisseur) {

        for (Produit achat : achats) {
            if (fournisseur.getId() == achat.getIdFournisseur()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Cette méthode permet à ce consommateur d’évaluer un fournisseur (celui reçu en paramètre)
     *
     * @param fournisseur le fournisseur a verifier
     * @param eval        L’évaluation donnée par cet utilisateur à l’utilisateur reçu en paramètre
     *                    (entre 1 et 5)
     * @throws NullPointerException si le fournisseur passé en paramètre est null
     * @throws ClassCastException   si le paramètre fournisseur n’est pas de type Fournisseur
     * @throws Exception            si le fournisseur passé en paramètre n’a jamais vendu de produit(s) à ce consommateur
     *                              ou si l’évaluation passée en paramètre (eval) est invalide.
     */
    @Override
    public void evaluer(Utilisateur fournisseur, int eval) throws Exception {
        if (fournisseur == null) {
            throw new NullPointerException();
        } else if (!(fournisseur instanceof Fournisseur)) {
            throw new ClassCastException();
        } else if (!isConsommateurAchterProduit(fournisseur)) {
            throw new Exception(MSG_ERR_CONSOMMATEUR_EVALUER_FOURNISSEUR);
        } else {
            fournisseur.ajouterEvaluation(eval);
        }
    }

    //--------------------------------------
    // METHODES D'INSTANCE PUBLIQUES
    //--------------------------------------

    //----------------------
    // GETTERS ET SETTERS
    //----------------------

    /**
     * retourne La liste des produits achetés par ce consommateur
     *
     * @return La liste des produits achetés par ce consommateur
     */
    public ArrayList<Produit> getAchats() {
        return achats;
    }

    /**
     * Cette methode ajoute un produit à la liste des achats (achats) de ce consommateur.
     *
     * @param produit     Le produit acheté
     * @param iqteAchetee La quantité achetée du produit
     * @throws ExceptionProduitInvalide si le produit donné en paramètre est null.
     * @throws Exception                si le produit n’est vendu par aucun fournisseur (idFournisseur = 0)
     *                                  ou si la quantité achetée est plus petite ou égale à 0 ou si elle est plus grande que la quantité en stock du
     *                                  produit
     */
    public void acheter(Produit produit, int iqteAchetee) throws Exception {

        if (produit == null) {
            throw new ExceptionProduitInvalide();
        } else if (produit.getIdFournisseur() == 0) {
            throw new Exception(MSG_ERR_FOURNISSEUR_NON_TROUVE);
        } else if (iqteAchetee <= 0 || (iqteAchetee > produit.getQuantite())) {
            throw new Exception(MSG_ERR_QUANTITE_INVALIDE);

        } else {
            Produit copieProduit = new Produit(produit);
            copieProduit.setQuantite(iqteAchetee);
            achats.add(copieProduit);
        }
    }

    /**
     * cette methode retourne une liste de tous les numéros d’identification (id) des fournisseurs de qui ce
     * consommateur a acheté un ou des produits
     *
     * @return une liste de tous les numéros d’identification (id) des fournisseurs de qui ce
     * consommateur a acheté un ou des produits
     */
    public ArrayList<Integer> fournisseurs() {
        ArrayList<Integer> fournisseursIds = new ArrayList<>();
        for (Produit achat : achats) {
            if (!fournisseursIds.contains(achat.getIdFournisseur())) {
                fournisseursIds.add(achat.getIdFournisseur());
            }
        }
        return fournisseursIds;
    }

    /**
     * Retourne une representation sous forme de chaine de caracteres
     * de ce consommateur.
     *
     * @return une representation sous forme de chaine de caracteres
     * de ce consommateur.
     */
    @Override
    public String toString() {
        return super.toString() + " - " + achats.size();
    }
}
