import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class TestsUtilisateur {
    private static int totalPoints = 0;
    private static final int TOTAL = 65;
    private static final int NOTE_MAX = 30;

    private static Produit p1 =
        new Produit ("table cuisine couleur verte", Produit.CATEGORIES[10]);
    private static Produit p2 =
        new Produit ("ipod gen 5", Produit.CATEGORIES[4]);
    private static Produit p3 =
        new Produit ("pot de fleur ceramique", Produit.CATEGORIES[16]);
    private static Produit p4 =
        new Produit ("sac a dos rouge", Produit.CATEGORIES[2]);
    private static Produit p5 =
        new Produit ("raquette de tennis", Produit.CATEGORIES[15]);
    private static Produit p6 =
        new Produit ("Antidote", Produit.CATEGORIES[9]);
    private static Produit p7 =
        new Produit ("huile a moteur", Produit.CATEGORIES[1]);
    private static Produit p8 =
        new Produit ("coussin pour chat", Produit.CATEGORIES[0]);
    private static Produit p9 =
        new Produit ("macbook pro", Produit.CATEGORIES[4]);
    private static Produit p10 =
        new Produit ("creme hydratante", Produit.CATEGORIES[14]);
    private static Produit p11 =
        new Produit ("chaise bistro", Produit.CATEGORIES[16]);
    private static Produit p12 =
        new Produit ("pot fleur plastique rouge", Produit.CATEGORIES[16]);
    private static Produit p13 =
        new Produit ("chaise bistro blanche", Produit.CATEGORIES[16]);
    private static Produit p14 =
        new Produit ("ensemble patio blanc", Produit.CATEGORIES[16]);
    private static Produit p15 =
        new Produit ("iphone 6", Produit.CATEGORIES[4]);
    private static Produit p16 =
        new Produit ("chaine stereo", Produit.CATEGORIES[4]);
    private static Produit p17 =
        new Produit ("potentiometre", Produit.CATEGORIES[4]);
    private static Produit p18 =
        new Produit ("chaine stereo haute fidelite", Produit.CATEGORIES[4]);
    private static Produit p19 =
        new Produit ("station d'accueil pour iPhone", Produit.CATEGORIES[4]);
    private static Produit p20 =
        new Produit ("mini speaker bluetooth", Produit.CATEGORIES[4]);

    private static ArrayList<Produit> lesProduits;
    private static ArrayList<Utilisateur> lesUtilisateurs;

    private static Utilisateur f1;
    private static Utilisateur f2;
    private static Utilisateur f3;
    private static Utilisateur f4;
    private static Utilisateur f5;
    private static Utilisateur c6;
    private static Utilisateur c7;
    private static Utilisateur c8;
    private static Utilisateur c9;
    private static Utilisateur c10;

    /**********************************
     * METHODES UTILITAIRES
     **********************************/

    private static void titre (String titre) {
        String s = "\n";
        for (int i = 0 ; i < titre.length() ; i++) {
            s = s + "-";
        }
        s = s + "\n" + titre.toUpperCase() + "\n";
        for (int i = 0 ; i < titre.length() ; i++) {
            s = s + "-";
        }

        System.out.println(s);
    }

    private static void err(String msg) {
        System.out.println(msg + "\n");
    }

    private static int ok(int points) {
        System.out.println("OK");
        return points;
    }

    private static void exceptionInattendue(Exception e) {
        System.out.println("ERREUR - " + e.getClass().getSimpleName()
            + " inattendue.");
    }

    private static void scoreInter(int points, int total) {
        totalPoints = totalPoints + points;
        System.out.println("\nPoints : " + points + " / " + total);
        System.out.println();
    }

    private static void afficherScore () {
        double note = (totalPoints * NOTE_MAX) / (double)TOTAL;
        double pointsTotal = totalPoints;
        double total = TOTAL;
        double noteMax = NOTE_MAX;

        System.out.printf (Locale.ENGLISH, "\nSCORE FINAL : %3.1f / %3.1f", pointsTotal, total);
        System.out.println();
        System.out.printf (Locale.ENGLISH, "\nNOTE FINALE : %3.1f / %3.1f", note, noteMax);
    }

    private static ArrayList<Produit> lister (Produit... produits) {
        ArrayList<Produit> liste = new ArrayList<>();
        liste.addAll(Arrays.asList(produits));
        return liste;
    }

    private static ArrayList<Utilisateur> lister (Utilisateur... utilisateurs) {
        ArrayList<Utilisateur> liste = new ArrayList<>();
        liste.addAll(Arrays.asList(utilisateurs));
        return liste;
    }

    private static ArrayList<String> lister (String... profil) {
        ArrayList<String> liste = new ArrayList<>();
        liste.addAll(Arrays.asList(profil));
        return liste;
    }

    private static ArrayList<Integer> lister (Integer... evals) {
        ArrayList<Integer> liste = new ArrayList<>();
        liste.addAll(Arrays.asList(evals));
        return liste;
    }

    private static boolean listesMemeContenu(ArrayList<String> l1,
                                             ArrayList<String> l2) {

        boolean memeContenu = false;
        int i;

        if (l1 != null && l2 != null && l1.size() == l2.size()) {
            memeContenu = true;

            for (i = 0 ; i < l2.size() ; i++) {
                l2.set(i, l2.get(i).toUpperCase());
            }

            i = 0;
            while (i < l1.size() && memeContenu) {
                memeContenu = l2.contains(l1.get(i).toUpperCase());
                i++;
            }

        } else if (l1 == null && l2 == null) {
            memeContenu = false;
        }

        return memeContenu;
    }

    private static boolean listesIntMemeContenu(ArrayList<Integer> l1,
                                                ArrayList<Integer> l2) {

        boolean memeContenu = false;
        int i;

        if (l1 != null && l2 != null && l1.size() == l2.size()) {
            memeContenu = true;

            i = 0;
            while (i < l1.size() && memeContenu) {
                memeContenu = l2.contains(l1.get(i));
                i++;
            }

        } else if (l1 == null && l2 == null) {
            memeContenu = false;
        }

        return memeContenu;
    }


    /**
     * Verifie que les attributs de c1 et c2 sont egaux, mais que c1 != c2 et
     * que c1.evaluations != c2.evaluations et que c1.achats != c2.achats
     * On suppose c1 et c2 non null.
     * @param c1 le consommateur a comparer avec c2
     * @param c2 l'autre consommateur a comparer avec c1
     * @return true si egaux, false sinon.
     */
    private static boolean consommateursEgaux(Consommateur c1, Consommateur c2) {
        boolean egaux;

        try {
            egaux = c1 != c2
                && c1.id == c2.id
                && c1.pseudo.equals(c2.pseudo)
                && c1.motPasse.equals(c2.motPasse)
                && c1.courriel.equals(c2.courriel)
                && c1.evaluations != c2.evaluations
                && c1.evaluations.equals(c2.evaluations)
                && c1.achats != c2.achats
                && c1.achats.equals(c2.achats);

        } catch (Exception e) {
            egaux = false;
        }

        return egaux;
    }

    /**
     * Verifie que les attributs de f1 et f2 sont egaux, mais que f1 != f2 et
     * que f1.evaluations != f2.evaluations et que f1.produits != f2.produits
     * On suppose f1 et f2 non null.
     * @param f1 le fournisseur a comparer avec f2
     * @param f2 l'autre fournisseur a comparer avec f1
     * @return true si egaux, false sinon.
     */
    private static boolean fournisseursEgaux(Fournisseur f1, Fournisseur f2) {
        boolean egaux;

        try {
            egaux = f1 != f2
                && f1.id == f2.id
                && f1.pseudo.equals(f2.pseudo)
                && f1.motPasse.equals(f2.motPasse)
                && f1.courriel.equals(f2.courriel)
                && f1.evaluations != f2.evaluations
                && f1.evaluations.equals(f2.evaluations)
                && f1.produits != f2.produits
                && f1.produits.equals(f2.produits);

        } catch (Exception e) {
            egaux = false;
        }

        return egaux;
    }

    /**
     * Initialisation de 20 produits.
     */
    private static void initProduits() {

        lesProduits = new ArrayList<>();

        Produit.intiSeqCodeProduit();
        System.out.println("Preparation test : initialisation de 20 produits :");

        p1 = new Produit ("table cuisine couleur verte", Produit.CATEGORIES[10]);
        p2 = new Produit ("ipod gen 5", Produit.CATEGORIES[4]);
        p3 = new Produit ("pot de fleur ceramique", Produit.CATEGORIES[16]);
        p4 = new Produit ("sac a dos rouge", Produit.CATEGORIES[2]);
        p5 = new Produit ("raquette de tennis", Produit.CATEGORIES[15]);
        p6 = new Produit ("Antidote", Produit.CATEGORIES[9]);
        p7 = new Produit ("huile a moteur", Produit.CATEGORIES[1]);
        p8 = new Produit ("coussin pour chat", Produit.CATEGORIES[0]);
        p9 = new Produit ("macbook pro", Produit.CATEGORIES[4]);
        p10 = new Produit ("creme hydratante", Produit.CATEGORIES[14]);
        p11 = new Produit ("chaise bistro", Produit.CATEGORIES[16]);
        p12 = new Produit ("pot fleur plastique rouge", Produit.CATEGORIES[16]);
        p13 = new Produit ("chaise bistro", Produit.CATEGORIES[16]);
        p14 = new Produit ("ensemble pation blanc", Produit.CATEGORIES[16]);
        p15 = new Produit ("iphone 6", Produit.CATEGORIES[4]);
        p16 = new Produit ("chaine stereo", Produit.CATEGORIES[4]);
        p17 = new Produit ("potentiometre", Produit.CATEGORIES[4]);
        p18 = new Produit ("chaine stereo haute fidelite", Produit.CATEGORIES[4]);
        p19 = new Produit ("station d'accueil pour iPhone", Produit.CATEGORIES[4]);
        p20 = new Produit ("mini speaker bluetooth", Produit.CATEGORIES[4]);

        lesProduits = lister(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13,
            p14, p15, p16, p17, p18, p19, p20);

        //Afficher les produits
        for (int i = 0 ; i < lesProduits.size() ; i++) {
            System.out.println("p" + (i + 1) + "\n" + lesProduits.get(i) + "\n--------");
        }
        System.out.println();
    }

    /**
     * Initialisation de 5 fournisseurs et 5 consommateurs.
     * @throws Exception si une erreur se produit.
     */
    private static void initFournisseursEtConsommateurs() throws Exception {

        Utilisateur.seqId = 1;

        System.out.println("Preparation test : "
            + "\n=> initialisation de 5 fournisseurs (f1 a f5) et de 5 consommateurs (c6 a c10)");

        f1 = new Fournisseur("p1", "m1", "c1");
        f2 = new Fournisseur("p2", "m2", "c2");
        f3 = new Fournisseur("p3", "m3", "c3");
        f4 = new Fournisseur("p4", "m4", "c4");
        f5 = new Fournisseur("p5", "m5", "c5");

        ((Fournisseur)f1).ajouterNouveauProduit(p1, 3, 789);
        ((Fournisseur)f1).ajouterNouveauProduit(p3, 12, 15);
        ((Fournisseur)f1).ajouterNouveauProduit(p8, 5, 23);
        ((Fournisseur)f1).ajouterNouveauProduit(p11, 10, 45);
        ((Fournisseur)f1).ajouterNouveauProduit(p13, 2, 78);
        ((Fournisseur)f1).ajouterNouveauProduit(p10, 4, 7);

        ((Fournisseur)f2).ajouterNouveauProduit(p3, 3, 14);
        ((Fournisseur)f2).ajouterNouveauProduit(p4, 1, 34);
        ((Fournisseur)f2).ajouterNouveauProduit(p12, 54, 5);
        ((Fournisseur)f2).ajouterNouveauProduit(p14, 1, 2340);

        ((Fournisseur)f3).ajouterNouveauProduit(p2, 10, 250);
        ((Fournisseur)f3).ajouterNouveauProduit(p6, 15, 99);
        ((Fournisseur)f3).ajouterNouveauProduit(p9, 3, 3000);
        ((Fournisseur)f3).ajouterNouveauProduit(p15, 3, 800);
        ((Fournisseur)f3).ajouterNouveauProduit(p16, 2, 1456);
        ((Fournisseur)f3).ajouterNouveauProduit(p10, 1, 4.5);

        ((Fournisseur)f4).ajouterNouveauProduit(p16, 1, 1610);
        ((Fournisseur)f4).ajouterNouveauProduit(p17, 17, 43);
        ((Fournisseur)f4).ajouterNouveauProduit(p18, 3, 2345);
        ((Fournisseur)f4).ajouterNouveauProduit(p19, 2, 67);
        ((Fournisseur)f4).ajouterNouveauProduit(p20, 10, 15);
        ((Fournisseur)f4).ajouterNouveauProduit(p7, 1, 12);
        ((Fournisseur)f4).ajouterNouveauProduit(p5, 1, 32);
        ((Fournisseur)f4).ajouterNouveauProduit(p10, 5, 17);

        c6 = new Consommateur("p6", "m6", "c6");
        c7 = new Consommateur("p7", "m7", "c7");
        c8 = new Consommateur("p8", "m8", "c8");
        c9 = new Consommateur("p9", "m9", "c9");
        c10 = new Consommateur("p10", "m10", "c10");

        lesUtilisateurs = lister(f1, f2, f3, f4, f5, c6, c7, c8, c9, c10);

        //Afficher les produits
        for (int i = 0 ; i < lesUtilisateurs.size() ; i++) {
            if (i < 5) {
                System.out.println("f" + (i + 1) + " : " + lesUtilisateurs.get(i));
            } else {
                System.out.println("c" + (i + 1) + " : " + lesUtilisateurs.get(i));
            }

        }
        System.out.println();

    }

    private static String listeToString(ArrayList<Produit> liste) {
        String s;
        if (liste == null) {
            s = "Liste null";
        } else if (liste.isEmpty()) {
            s = "Liste vide";
        } else {
            s = "[\n";
            for (Produit p : liste) {
                s = s + p + "\n";
            }
            s = s + "]\n";
        }
        return s;
    }

    /**********************************
     * METHODES DE TESTS
     **********************************/

    /**
     * Tests des constructeurs d'initialisation, des getters et des setters.
     */
    public static void testsConstructeursInitGettersSetters () {
        int points = 0;
        int total = 12;
        Consommateur cs1;
        Fournisseur fs1 ;
        int idAttendu = 1;
        String sAttendu;
        int trouve;
        String sTrouve;
        ArrayList<Integer> evalAttendu;
        ArrayList<Integer> evalTrouve;
        ArrayList<Produit> prdAttendu;
        ArrayList<Produit> prdTrouve;

        titre("tests - constructeurs d'initialisation / getters / setters");

        try {

            try {
                System.out.println("Construction d'un consommateur cs1 "
                    + "(\"pseudo1\", \"pass1\", \"mail1\")\n");
                cs1 = new Consommateur("pseudo1", "pass1", "mail1");

                try {
                    idAttendu = 1;
                    System.out.print("Verification de cs1.getId() -> "
                        + idAttendu + "... ");
                    trouve = cs1.getId();

                    if (trouve == idAttendu) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + idAttendu
                            + "\n\n*** Trouve *** :\n" + trouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    sAttendu = "pseudo1";
                    System.out.print("Verification de cs1.getPseudo() -> "
                        + sAttendu + "... ");

                    sTrouve = cs1.getPseudo();

                    if (sAttendu.equals(sTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + sAttendu
                            + "\n\n*** Trouve *** :\n" + sTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    sAttendu = "pass1";
                    System.out.print("Verification de cs1.getMotPasse() -> "
                        + sAttendu + "... ");

                    sTrouve = cs1.getMotPasse();

                    if (sAttendu.equals(sTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + sAttendu
                            + "\n\n*** Trouve *** :\n" + sTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    sAttendu = "mail1";
                    System.out.print("Verification de cs1.getCourriel() -> "
                        + sAttendu + "... ");

                    sTrouve = cs1.getCourriel();

                    if (sAttendu.equals(sTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + sAttendu
                            + "\n\n*** Trouve *** :\n" + sTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    evalAttendu = new ArrayList<>();
                    System.out.print("Verification de cs1.getEvaluations() -> "
                        + "liste vide" + "... ");

                    evalTrouve = cs1.getEvaluations();

                    if (evalAttendu.equals(evalTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + evalAttendu
                            + "\n\n*** Trouve *** :\n" + evalTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    prdAttendu = new ArrayList<>();
                    System.out.print("Verification de cs1.getAchats() -> "
                        + "liste vide" + "... ");

                    prdTrouve = cs1.getAchats();

                    if (prdAttendu.equals(prdTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + prdAttendu
                            + "\n\n*** Trouve *** :\n" + prdTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

            } catch (Exception e) {
                exceptionInattendue(e);
            }

            //-----------------------------

            try {
                System.out.println("\nConstruction d'un fournisseur fs1 "
                    + "(\"pseudo2\", \"pass2\", \"mail2\")\n");
                fs1 = new Fournisseur("pseudo2", "pass2", "mail2");

                try {
                    idAttendu = 2;
                    System.out.print("Verification de fs1.getId() -> "
                        + idAttendu + "... ");
                    trouve = fs1.getId();

                    if (trouve == idAttendu) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + idAttendu
                            + "\n\n*** Trouve *** :\n" + trouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    evalAttendu = new ArrayList<>();
                    System.out.print("Verification de fs1.getEvaluations() -> "
                        + "liste vide" + "... ");

                    evalTrouve = fs1.getEvaluations();

                    if (evalAttendu.equals(evalTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + evalAttendu
                            + "\n\n*** Trouve *** :\n" + evalTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    prdAttendu = new ArrayList<>();
                    System.out.print("Verification de fs1.getProduits() -> "
                        + "liste vide" + "... ");

                    prdTrouve = fs1.getProduits();

                    if (prdAttendu.equals(prdTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + prdAttendu
                            + "\n\n*** Trouve *** :\n" + prdTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    sAttendu = "autrePseudo";
                    System.out.print("Verification de fs1.setPseudo(\"autrePseudo\")... ");

                    fs1.setPseudo(sAttendu);
                    sTrouve = fs1.pseudo;

                    if (sAttendu.equals(sTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + sAttendu
                            + "\n\n*** Trouve *** :\n" + sTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    sAttendu = "autrePass";
                    System.out.print("Verification de fs1.setMotPasse(\"autrePass\")... ");

                    fs1.setMotPasse(sAttendu);
                    sTrouve = fs1.motPasse;

                    if (sAttendu.equals(sTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + sAttendu
                            + "\n\n*** Trouve *** :\n" + sTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    sAttendu = "autreMail";
                    System.out.print("Verification de fs1.setCourriel(\"autreMail\")... ");

                    fs1.setCourriel(sAttendu);
                    sTrouve = fs1.courriel;

                    if (sAttendu.equals(sTrouve)) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + sAttendu
                            + "\n\n*** Trouve *** :\n" + sTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }


    /**
     * Tests des constructeurs de copie.
     */
    public static void testsConstructeursDeCopie() {
        int points = 0;
        int total = 2;
        Consommateur cs1;
        Consommateur cs2;
        Fournisseur fs1 ;
        Fournisseur fs2;

        titre("tests - constructeurs de copie");

        try {

            try {
                System.out.println("Construction d'un consommateur cs1 "
                    + "(\"pseudo1\", \"pass1\", \"mail1\")");
                cs1 = new Consommateur("pseudo1", "pass1", "mail1");
                System.out.println("Construction d'une copie (cs2) du consommateur cs1");
                cs2 = new Consommateur(cs1);

                System.out.print("\nVerification de la copie cs2... ");

                if (consommateursEgaux(cs1, cs2)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR : la copie ne respecte pas toutes les specifications.");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                System.out.println("\nConstruction d'un fournisseur fs1 "
                    + "(\"pseudo2\", \"pass2\", \"mail2\")");
                fs1 = new Fournisseur("pseudo2", "pass2", "mail2");
                System.out.println("Construction d'une copie (fs2) du fournisseur fs1");
                fs2 = new Fournisseur(fs1);

                System.out.print("\nVerification de la copie fs2... ");

                if (fournisseursEgaux(fs1, fs2)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR : la copie ne respecte pas toutes les specifications.");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }

    /**
     * Tests des methodes equals.
     */
    public static void testsEquals() {
        int points = 0;
        int total = 5;

        boolean bAttendu;
        boolean bTrouve;
        Utilisateur uc1;
        Utilisateur uc2;
        Utilisateur uf1 ;

        titre("tests - equals");

        try {

            try {
                System.out.println("Construction d'un utilisateur(consommateur) uc1 "
                    + "(\"pseudo1\", \"pass1\", \"mail1\")");
                uc1 = new Consommateur("pseudo1", "pass1", "mail1");

                System.out.println("Construction d'un utilisateur(consommateur) uc2 "
                    + "(\"pseudo1\", \"pass1\", \"mail1\")");
                uc2 = new Consommateur("pseudo1", "pass1", "mail1");

                System.out.println("Construction d'un utilisateur(fournisseur) uf1 "
                    + "(\"pseudo1\", \"pass1\", \"mail1\")");
                uf1 = new Fournisseur("pseudo1", "pass1", "mail1");

                bAttendu = false;
                System.out.print("\nVerification que uc1.equals(uc2) retourne "
                    + bAttendu + "... ");
                bTrouve = uc1.equals(uc2);

                if (!bTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + bAttendu
                        + "\n\n*** Trouve *** :\n" + bTrouve + "\n");
                }

                bAttendu = false;
                System.out.print("Verification que uc1.equals(uf1) retourne "
                    + bAttendu + "... ");
                bTrouve = uc1.equals(uf1);

                if (!bTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + bAttendu
                        + "\n\n*** Trouve *** :\n" + bTrouve + "\n");
                }

                bAttendu = true;
                System.out.println("\nAffectation : uc2 = uc1");
                uc2 = uc1;
                System.out.print("\nVerification que uc1.equals(uc2) retourne "
                    + bAttendu + "... ");
                bTrouve = uc1.equals(uc2);

                if (bTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + bAttendu
                        + "\n\n*** Trouve *** :\n" + bTrouve + "\n");
                }

                try {
                    bAttendu = false;
                    System.out.print("Verification que uc1.equals(\"toto\") retourne "
                        + bAttendu + "... ");
                    bTrouve = uc1.equals("toto");

                    if (!bTrouve) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + bAttendu
                            + "\n\n*** Trouve *** :\n" + bTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }

                try {
                    bAttendu = false;
                    System.out.print("Verification que uc1.equals(null) retourne "
                        + bAttendu + "... ");
                    bTrouve = uc1.equals(null);

                    if (!bTrouve) {
                        points = points + ok(1);
                    } else {
                        err("ERREUR :\n\n" + "*** Attendu *** :\n" + bAttendu
                            + "\n\n*** Trouve *** :\n" + bTrouve + "\n");
                    }
                } catch (Exception e) {
                    exceptionInattendue(e);
                }



            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }

    /**
     * Tests des methodes ajouter nouveau produit et obtenir produit.
     */
    public static void testsAjouterNouveauProduitEtObtenirProduit() {
        int points = 0;
        int total = 9;

        int iAttendu;
        int iTrouve;
        ArrayList<Produit> lAttendu;
        ArrayList<Produit> lTrouve;
        boolean bAttendu;
        boolean bTrouve;
        Produit pAttendu;
        Produit pTrouve;
        Utilisateur uf1 ;

        titre("tests - ajouter nouveau produit et obtenir produit ");

        try {

            initProduits();

            System.out.println("Construction d'un utilisateur(fournisseur) uf1 "
                + "(\"pseudo1\", \"pass1\", \"mail1\")");
            uf1 = new Fournisseur("pseudo1", "pass1", "mail1");


            try {
                System.out.print("\nTentative d'ajout d'un produit null au fournisseur uf1... ");
                ((Fournisseur)uf1).ajouterNouveauProduit(null, 5, 1345.99);

                err("ERREUR : devrait lever une ExceptionProduitInvalide");
            } catch (ExceptionProduitInvalide e) {
                points = points + ok(1);
            } catch (Exception e) {
                err("ERREUR : devrait lever une ExceptionProduitInvalide et "
                    + "non une " + e.getClass().getSimpleName());
            }

            try {
                System.out.print("Tentative d'ajout d'un produit de quantite = 0 au fournisseur uf1... ");
                ((Fournisseur)uf1).ajouterNouveauProduit(p1, 0, 1345.99);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("Tentative d'ajout d'un produit de prix = 0 au fournisseur uf1... ");
                ((Fournisseur)uf1).ajouterNouveauProduit(p1, 5, 0);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                iAttendu = 0;
                System.out.print("Verification que la liste des produits de uf1 est vide... ");
                iTrouve = ((Fournisseur)uf1).produits.size();
                if (iAttendu == iTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + iAttendu
                        + "\n\n*** Trouve *** :\n" + iTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            System.out.println("\nConstruction d'un utilisateur(fournisseur) uf1 "
                + "(\"pseudo1\", \"pass1\", \"mail1\")");
            uf1 = new Fournisseur("pseudo1", "pass1", "mail1");

            try {
                lAttendu = lister(p1, p2, p3, p4, p5);

                System.out.print("\nAjout des produits p1 a p5 (avec prix et "
                    + "qtes valides) au fournisseur uf1... ");
                ((Fournisseur)uf1).ajouterNouveauProduit(p1, 1, 1);
                ((Fournisseur)uf1).ajouterNouveauProduit(p2, 1, 1);
                ((Fournisseur)uf1).ajouterNouveauProduit(p3, 1, 1);
                ((Fournisseur)uf1).ajouterNouveauProduit(p4, 1, 1);
                ((Fournisseur)uf1).ajouterNouveauProduit(p5, 1, 1);

                lTrouve = ((Fournisseur)uf1).produits;

                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + listeToString(lAttendu)
                        + "\n\n*** Trouve *** :\n" + listeToString(lTrouve) + "\n");
                }

                System.out.print("Verification que les produits ajoutes sont des copies... ");
                bAttendu = false;
                bTrouve = p1 == ((Fournisseur)uf1).produits.get(0);

                if (bAttendu == bTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR : p1 == ((Fournisseur)uf1).produits.get(0) s'evalue a true et ne devrait pas");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }


            try {
                pAttendu = p4;
                System.out.print("((Fournisseur)uf1).obtenirProduit(4)... ");
                pTrouve = ((Fournisseur)uf1).obtenirProduit(4);
                if (pAttendu.equals(pTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + pAttendu
                        + "\n\n*** Trouve *** :\n" + pTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                pAttendu = null;
                System.out.print("((Fournisseur)uf1).obtenirProduit(6) - produit inexistant... ");
                pTrouve = ((Fournisseur)uf1).obtenirProduit(6);
                if (pAttendu == pTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + pAttendu
                        + "\n\n*** Trouve *** :\n" + pTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }


            try {
                System.out.print("Tentative d'ajout d'un produit existant (p3) au fournisseur uf1... ");
                ((Fournisseur)uf1).ajouterNouveauProduit(p3, 1, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }


        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }

    /**
     * Tests des methodes ajouter evaluation et evaluation moyenne.
     */
    public static void testsAjouterEvalEtEvalMoyenne() {
        int points = 0;
        int total = 4;
        double dAttendu;
        double dTrouve;
        ArrayList<Integer> lAttendu;
        ArrayList<Integer> lTrouve;
        Utilisateur u1 ;

        titre("tests - ajouter evaluation et evaluation moyenne");

        try {
            System.out.println("Preparation des tests : creation d'un utilisateur u1.");
            u1 = new Consommateur("p1", "m1", "c1");

            try {
                System.out.print("\nu1.ajouterEvaluation(0) - eval invalide... ");
                u1.ajouterEvaluation(0);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                dAttendu = 0;
                System.out.print("u1.evaluationMoyenne()... ");
                dTrouve = u1.evaluationMoyenne();
                if (dAttendu == dTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + dAttendu
                        + "\n\n*** Trouve *** :\n" + dTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            System.out.println("\nPreparation des tests : ajout de 5 evaluations a u1 (5, 4, , 1, 3 , 3).");
            u1.ajouterEvaluation(5);
            u1.ajouterEvaluation(4);
            u1.ajouterEvaluation(1);
            u1.ajouterEvaluation(3);
            u1.ajouterEvaluation(3);

            try {
                lAttendu = lister(5, 4, 1, 3, 3);
                System.out.print("\nVerification de la liste des evaluations de u1... ");
                lTrouve = u1.evaluations;
                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                dAttendu = (5 + 4 + 1 + 3 + 3) / 5.0;
                dAttendu = (double)Math.round(dAttendu * 100) / 100;
                System.out.print("u1.evaluationMoyenne()... ");
                dTrouve = u1.evaluationMoyenne();

                if (dAttendu == dTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + dAttendu
                        + "\n\n*** Trouve *** :\n" + dTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }


    /**
     * tests de la methodes acheter.
     */
    public static void testsAcheter() {
        int points = 0;
        int total = 9;

        int iAttendu;
        int iTrouve;
        ArrayList<Produit> lAttendu;
        ArrayList<Produit> lTrouve;

        titre("tests - acheter");

        try {

            initProduits();
            initFournisseursEtConsommateurs();

            try {
                System.out.print("((Consommateur)c6).acheter(null, 1)... ");
                ((Consommateur)c6).acheter(null, 1);
                err("ERREUR : devrait lever une ExceptionProduitInvalide");
            } catch (ExceptionProduitInvalide e) {
                points = points + ok(1);
            } catch (Exception e) {
                err("ERREUR : devrait lever une ExceptionProduitInvalide et "
                    + "non une " + e.getClass().getSimpleName());
            }

            try {
                Produit produitNonVendu = new Produit ("produit", Produit.CATEGORIES[10]);
                System.out.print("((Consommateur)c6).acheter(produitNonVendu, 1)... ");
                ((Consommateur)c6).acheter(produitNonVendu, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(8), 0) - qte = 0... ");
                ((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(8), 0);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }


            try {
                System.out.print("((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(14), 2) - qte insuffisante... ");
                ((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(14), 2);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                lAttendu = new ArrayList<>();
                System.out.print("Verification que la liste d'achat de c6 est vide... ");
                lTrouve = ((Consommateur)c6).achats;
                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + listeToString(lAttendu)
                        + "\n\n*** Trouve *** :\n" + listeToString(lTrouve) + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }


            try {
                lAttendu = lister(p1);
                System.out.print("((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 3)... ");
                ((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 3);
                lTrouve = ((Consommateur)c6).achats;

                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + listeToString(lAttendu)
                        + "\n\n*** Trouve *** :\n" + listeToString(lTrouve) + "\n");
                }

            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                iAttendu = 3;
                System.out.print("Verification que la qte du produit ajoute dans la liste d'achats est " + iAttendu + "... ");
                lTrouve = ((Consommateur)c6).achats;
                iTrouve = lTrouve.get(0).getQuantite();
                if (iAttendu == iTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + iAttendu
                        + "\n\n*** Trouve *** :\n" + iTrouve + "\n");
                }

            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                lAttendu = lister(p1, p3);
                System.out.print("((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(3), 1)... ");
                ((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(3), 1);
                lTrouve = ((Consommateur)c6).achats;

                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + listeToString(lAttendu)
                        + "\n\n*** Trouve *** :\n" + listeToString(lTrouve) + "\n");
                }

            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                iAttendu = 1;
                System.out.print("Verification que la qte du produit ajoute dans la liste d'achats est " + iAttendu + "... ");
                lTrouve = ((Consommateur)c6).achats;
                iTrouve = lTrouve.get(1).getQuantite();
                if (iAttendu == iTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + iAttendu
                        + "\n\n*** Trouve *** :\n" + iTrouve + "\n");
                }

            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }


    /**
     * Tests de la methode vendre.
     */
    public static void testsVendre() {
        int points = 0;
        int total = 5;

        int iAttendu;
        int iTrouve;
        ArrayList<Produit> lAttendu;
        ArrayList<Produit> lTrouve;

        titre("tests - vendre");

        try {
            initProduits();
            initFournisseursEtConsommateurs();

            try {
                System.out.print("((Fournisseur)f1).vendre(9, 1) - produit non vendu par ce fournisseur... ");
                ((Fournisseur)f1).vendre(9, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("((Fournisseur)f1).vendre(13, 0) - qte = 0... ");
                ((Fournisseur)f1).vendre(13, 0);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("((Fournisseur)f1).vendre(13, 3) - qte insuffisante... ");
                ((Fournisseur)f1).vendre(13, 3);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                iAttendu = 0;
                System.out.print("((Fournisseur)f1).vendre(10, 4) - verification de la quantite en inventaire... ");
                ((Fournisseur)f1).vendre(10, 4);
                iTrouve = ((Fournisseur)f1).produits.get(5).getQuantite();
                if (iAttendu == iTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + iAttendu
                        + "\n\n*** Trouve *** :\n" + iTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                iAttendu = 4;
                System.out.print("((Fournisseur)f4).vendre(10, 1) - verification de la quantite en inventaire... ");
                ((Fournisseur)f4).vendre(10, 1);
                iTrouve = ((Fournisseur)f4).produits.get(7).getQuantite();
                if (iAttendu == iTrouve) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + iAttendu
                        + "\n\n*** Trouve *** :\n" + iTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }


    /**
     * Tests de la methode fournisseurs.
     */
    public static void testsFournisseurs() {
        int points = 0;
        int total = 2;

        ArrayList<Integer> lAttendu;
        ArrayList<Integer> lTrouve;

        titre("tests - fournisseurs");

        try {
            initProduits();
            initFournisseursEtConsommateurs();

            try {
                lAttendu = new ArrayList<>();
                System.out.print("((Consommateur)c6).fournisseurs() - Verification de la liste retournee... ");
                lTrouve = ((Consommateur)c6).fournisseurs();

                if (listesIntMemeContenu(lAttendu, lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            System.out.println("\nPreparation des tests : achats du consommateur c6 : ");
            System.out.println(
                "((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 1);\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(3), 1);\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(12), 1);\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(16), 1);\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(20), 1);\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(7), 1);");

            ((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(3), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(12), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(16), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(20), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(7), 1);

            try {
                lAttendu = lister(1, 2, 4);
                System.out.print("\n((Consommateur)c6).fournisseurs() - Verification de la liste retournee... ");
                lTrouve = ((Consommateur)c6).fournisseurs();
                if (listesIntMemeContenu(lAttendu, lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }

    /**
     * Tests des methodes de compilerProfil.
     */
    public static void testsCompilerProfil() {
        int points = 0;
        int total = 5;

        int iAttendu;
        int iTrouve;
        ArrayList<String> lAttendu;
        ArrayList<String> lTrouve;

        titre("tests - compiler profil");

        try {
            initProduits();
            initFournisseursEtConsommateurs();
            System.out.println("Preparation des tests :");
            System.out.println("((Fournisseur)f1).vendre(8, 5)");
            System.out.println("((Fournisseur)f1).vendre(10, 4)");
            ((Fournisseur)f1).vendre(8, 5);  //qte est devenue 0
            ((Fournisseur)f1).vendre(10, 4); //qte est devenue 0

            try {
                lAttendu = new ArrayList<>();
                System.out.print("\nf5.compilerProfil()... ");
                lTrouve = f5.compilerProfil();
                if (listesMemeContenu(lAttendu, lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                lAttendu = new ArrayList<>();
                lAttendu.add(Produit.CATEGORIES[4]);
                lAttendu.add(Produit.CATEGORIES[1]);
                lAttendu.add(Produit.CATEGORIES[15]);
                lAttendu.add(Produit.CATEGORIES[14]);

                System.out.print("f4.compilerProfil()... ");
                lTrouve = f4.compilerProfil();
                if (listesMemeContenu(lAttendu, lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                lAttendu = new ArrayList<>();
                lAttendu.add(Produit.CATEGORIES[10]);
                lAttendu.add(Produit.CATEGORIES[16]);
                System.out.print("f1.compilerProfil()... ");
                lTrouve = f1.compilerProfil();
                if (listesMemeContenu(lAttendu, lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                lAttendu = new ArrayList<>();
                System.out.print("c6.compilerProfil()... ");  //c6 n'a aucun achat
                lTrouve = c6.compilerProfil();
                if (listesMemeContenu(lAttendu, lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            System.out.println("\nPreparation des tests :");
            System.out.println(
                "((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 1)  \n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(3), 1)\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f3).obtenirProduit(9), 1)\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f3).obtenirProduit(6), 1)\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(20), 1)\n" +
                    "((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(5), 1)");

            ((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f2).obtenirProduit(3), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f3).obtenirProduit(9), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f3).obtenirProduit(6), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(20), 1);
            ((Consommateur)c6).acheter(((Fournisseur)f4).obtenirProduit(5), 1);

            try {
                lAttendu = new ArrayList<>();
                lAttendu.add(Produit.CATEGORIES[4]);
                lAttendu.add(Produit.CATEGORIES[10]);
                lAttendu.add(Produit.CATEGORIES[16]);
                lAttendu.add(Produit.CATEGORIES[9]);
                lAttendu.add(Produit.CATEGORIES[15]);

                System.out.print("\nc6.compilerProfil()... ");
                lTrouve = c6.compilerProfil();
                if (listesMemeContenu(lAttendu, lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }

    /**
     * Tests des methodes evaluer.
     */
    public static void testsEvaluer() {
        int points = 0;
        int total = 12;

        int iAttendu;
        int iTrouve;
        ArrayList<Integer> lAttendu;
        ArrayList<Integer> lTrouve;

        titre("tests - evaluer");

        try {
            initProduits();
            initFournisseursEtConsommateurs();

            //CONSOMMATEUR

            try {
                System.out.print("c6.evaluer(null, 1)... ");  //fournisseur a evaluer = null
                c6.evaluer(null, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("c6.evaluer(c7, 1)... ");  //c7 n'est pas de type fournisseur
                c6.evaluer(c7, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("c6.evaluer(f1, 1)... ");  //c6 n'a pas achete de produits de f1
                c6.evaluer(f1, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }


            System.out.println("\nPreparation des tests : ((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 1)");
            ((Consommateur)c6).acheter(((Fournisseur)f1).obtenirProduit(1), 1);

            try {
                System.out.print("c6.evaluer(f1, 6)... ");  //eval invalide
                c6.evaluer(f1, 6);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                lAttendu = lister(4);
                System.out.print("c6.evaluer(f1, 4) - verification de la liste des evaluations de f1... ");
                c6.evaluer(f1, 4);
                lTrouve = f1.evaluations;
                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                lAttendu = lister(4, 2);
                System.out.print("c6.evaluer(f1, 2) - verification de la liste des evaluations de f1... ");
                c6.evaluer(f1, 2);
                lTrouve = f1.evaluations;
                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            //FOURNISSEUR

            try {
                System.out.print("f2.evaluer(null, 1)... ");  //consommateur a evaluer = null
                f2.evaluer(null, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("f1.evaluer(f2, 1)... ");  //f2 n'est pas de type Consommateur
                f1.evaluer(f2, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("f2.evaluer(c6, 1)... ");  //f2 n'a pas vendu de produits a c6
                f2.evaluer(c6, 1);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                System.out.print("f1.evaluer(c6, 0)... ");  //eval invalide
                f1.evaluer(c6, 0);
                err("ERREUR : devrait lever une Exception");
            } catch (ExceptionProduitInvalide e) {
                err("ERREUR : devrait lever une Exception et "
                    + "non une " + e.getClass().getSimpleName());
            } catch (Exception e) {
                points = points + ok(1);
            }

            try {
                lAttendu = lister(3);
                System.out.print("f1.evaluer(c6, 3) - verification de la liste des evaluations de c6... ");
                f1.evaluer(c6, 3);
                lTrouve = c6.evaluations;
                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

            try {
                lAttendu = lister(3, 2);
                System.out.print("f1.evaluer(c6, 2) - verification de la liste des evaluations de c6... ");
                f1.evaluer(c6, 2);
                lTrouve = c6.evaluations;
                if (lAttendu.equals(lTrouve)) {
                    points = points + ok(1);
                } else {
                    err("ERREUR :\n\n" + "*** Attendu *** :\n" + lAttendu
                        + "\n\n*** Trouve *** :\n" + lTrouve + "\n");
                }
            } catch (Exception e) {
                exceptionInattendue(e);
            }

        } catch (Exception e) {
            exceptionInattendue(e);
        }
        scoreInter(points, total);
    }

    /**
     * Execution des methodes de tests.
     */
    public static void main(String[] args) {

        //Methodes de tests
        testsConstructeursInitGettersSetters();
        testsConstructeursDeCopie();
        testsEquals();
        testsAjouterNouveauProduitEtObtenirProduit();
        testsAjouterEvalEtEvalMoyenne();
        testsAcheter();
        testsVendre();
        testsFournisseurs();
        testsCompilerProfil();
        testsEvaluer();

        //Afficher la note
        afficherScore ();
        System.out.println ("\n\n\n");
    }

}
