import java.util.ArrayList;


/**
 * Cette classe modelise un utilisateur de site.
 *
 * @author yassine
 * @version Automne 2020
 */
public abstract class Utilisateur {

    //-----------------------------------
    // ATTRIBUTS ET CONSTANTES DE CLASSE
    //-----------------------------------

    //messages d'erreurs
    public static final String MSG_ERR_VAL_EVAL_INVALIDE = "Erreur, l'evaluation doit " +
        "etre un nombre entier entre 1 et 5 inclusivement.";

    //Variable de classe representant une sequence pour l'assignation d'un
    //id d'utilisateur unique.
    private static int seqId = 1;

    //-----------------------------------
    // ATTRIBUTS D'INSTANCE
    //-----------------------------------

    private int id;         // identifiant unique pour l'utilisateur

    private String pseudo;  // Le nom d'utilisateur

    private String motPasse;// le Le mot de passe de l'utilisateur

    private String courriel;// Le courriel de l'utilisateur

    private ArrayList<Integer> evaluations; // La liste des évaluations reçues des autres utilisateurs

    //-------------------
    // CONSTRUCTEURS
    //-------------------

    /**
     * Construit un Utilisateur avec le pseudo,le motPasse et le courriel.
     * Le seqId est assigne automatiquement (entier >= 1).
     *
     * @param pseudo   Le pseudonyme de l'utilisateur supposé valide (non null, non vides,
     *                 correctement formés, etc.)
     * @param motPasse Le mot de passe de cet utilisateur supposé valide (non null, non vides,
     *                 * correctement formés, etc.)
     * @param courriel Le courriel de cet utilisateur supposé valide (non null, non vides,
     *                 *      * correctement formés, etc.)
     */
    public Utilisateur(String pseudo, String motPasse, String courriel) {
        this.pseudo = pseudo;
        this.motPasse = motPasse;
        this.courriel = courriel;
        this.evaluations = new ArrayList<>();
        this.id = seqId;
        seqId++;
    }

    /**
     * Constructeur de copie.
     *
     * @param utilisateur l'utilisateur a copier.
     */
    public Utilisateur(Utilisateur utilisateur) {
        this.id = utilisateur.id;
        this.pseudo = utilisateur.pseudo;
        this.motPasse = utilisateur.motPasse;
        this.courriel = utilisateur.courriel;
        this.evaluations = (ArrayList<Integer>) utilisateur.evaluations.clone();
    }

    //--------------------------------------
    // METHODES D'INSTANCE PUBLIQUES ABSTRAITES
    //--------------------------------------

    /**
     * methode abstraite retourne une liste de chaines de caractères représentant le profil de l'utilisateur
     *
     * @return une liste de chaines de caractères représentant le profil de l'utilisateur
     */
    public abstract ArrayList<String> compilerProfil();


    /**
     * methode abstraite permet à un utilisateur d’évaluer un autre utilisateur
     *
     * @param utilisateur L’utilisateur à évaluer par cet utilisateur
     * @param eval        L’évaluation donnée par cet utilisateur à l’utilisateur reçu en paramètre
     *                    (entre 1 et 5)
     * @throws Exception si l'utilisateur est null ou eval > 5 ou eval < 1
     */
    public abstract void evaluer(Utilisateur utilisateur, int eval) throws Exception;


    //--------------------------------------
    // METHODES D'INSTANCE PUBLIQUES ABSTRAITES
    //--------------------------------------

    //----------------------
    // GETTERS ET SETTERS
    //----------------------


    /**
     * retourne id de l'utilisateur
     *
     * @return id de l'utilisateur
     */
    public int getId() {
        return id;
    }


    /**
     * retourne le nom d'utilisateur
     *
     * @return nom d'utilisateur
     */
    public String getPseudo() {
        return pseudo;
    }


    /**
     * retourne le mot de passe de l'utilisateur
     *
     * @return le mot de passe de l'utilisateur
     */
    public String getMotPasse() {
        return motPasse;
    }

    /**
     * retourne Le courriel de l'utilisateur
     *
     * @return Le courriel de l'utilisateur
     */
    public String getCourriel() {
        return courriel;
    }

    /**
     * retourne La liste des évaluations reçues des autres utilisateurs
     *
     * @return La liste des évaluations reçues des autres utilisateurs
     */
    public ArrayList<Integer> getEvaluations() {
        return evaluations;
    }

    /**
     * modifie Le nom d'utilisateur de cet utilisateur
     *
     * @param pseudo Le nom d'utilisateur
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     * modifie Le mot de passe de cet utilisateur
     *
     * @param motPasse Le mot de passe de cet utilisateur
     */
    public void setMotPasse(String motPasse) {
        this.motPasse = motPasse;
    }

    /**
     * modifie Le courriel de cet utilisateur
     *
     * @param courriel Le courriel de cet utilisateur
     */
    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    /**
     * calcule la moyenne de toutes les évaluations de cet utilisateur arrondie à deux décimales
     * Si l’utilisateur n’a encore reçu aucune évaluation, la moyenne retournée est 0
     *
     * @return la moyenne de toutes les évaluations de cet utilisateur arrondie à deux décimales
     * Si l’utilisateur n’a encore reçu aucune évaluation, la moyenne retournée est 0
     */
    public double evaluationMoyenne() {
        if (this.evaluations.isEmpty()) {
            return 0;
        } else {
            int somme = 0;
            for (int evaluation : evaluations) {
                somme += evaluation;
            }
            float moyenne = (float) somme / evaluations.size();
            return (double) Math.round(moyenne * 100) / 100;
        }
    }

    /**
     * permet d’ajouter une nouvelle évaluation reçue à la liste des évaluations de cet utilisateur.
     *
     * @param eval L’évaluation à ajouter à la liste d’évaluations de cet utilisateur. Une
     *             évaluation valide doit être entre 1 et 5 inclusivement.
     * @throws Exception l’évaluation donnée en paramètre n’est pas valide.
     */
    public void ajouterEvaluation(int eval) throws Exception {
        if (eval <= 5 && eval >= 1) {
            evaluations.add(eval);
        } else {
            throw new Exception(MSG_ERR_VAL_EVAL_INVALIDE);
        }
    }

    /**
     * Redefinition de la methode equals. Retourne vrai si les deux utilisateurs
     * sont egaux, faux sinon. Deux utilisateur sont consideres egaux s’ils ont le même id
     *
     * @param autreUtilisateur l'utilisateur dont on veut tester l'egalite avec cet
     *                         utilisateur.
     * @return true si les deux utilisateurs compares sont egaux, false sinon.
     */
    @Override
    public boolean equals(Object autreUtilisateur) {
        return autreUtilisateur != null
            && this.getClass() == autreUtilisateur.getClass()
            && this.id == ((Utilisateur) autreUtilisateur).id;
    }

    /**
     * Retourne une representation de cet utilisateur sous forme d'une chaine de
     * caracteres (son id, son pseudonyme, son mot de passe, son courriel et le
     * nombre de ses evaluations reçues).
     *
     * @return une representation de cet utilisateur sous forme d'une chaine de
     * caracteres.
     */
    @Override
    public String toString() {
        return id + " : " + pseudo + " - " + motPasse + " - " + courriel
            + " - " + evaluations.size();
    }
}
