import java.util.ArrayList;

/**
 * Cette classe contient les méthodes de gestion du site Amizone
 *
 * @author
 * @version Automne 2020
 */
public class Amizone {

    //-----------------------------------
    // ATTRIBUTS ET CONSTANTES DE CLASSE
    //-----------------------------------

    //messages d'erreurs
    public final static String MSG_ERR_UTILIS_NULL = "Erreur, utilisateur null.";
    public final static String MSG_ERR_FOURN_AUCUN_PRODUIT =
        "Erreur, ce fournisseur ne vend aucun produit.";
    public final static String MSG_ERR_UTILIS_EXISTANT =
        "Erreur, cet utilisateur existe deja.";


    //-----------------------------------
    // ATTRIBUTS D'INSTANCE
    //-----------------------------------
    private ArrayList<Utilisateur> utilisateurs; // liste des utilisateurs d’Amizone.

    //-------------------
    // CONSTRUCTEURS
    //-------------------

    /**
     * Construit une instance {@link Amizone} avec liste des utilisateurs vide
     */
    public Amizone() {
        utilisateurs = new ArrayList<>();
    }

    //--------------------------------------
    // METHODES D'INSTANCE PUBLIQUES
    //--------------------------------------

    //----------------------
    // GETTERS ET SETTERS
    //----------------------

    /**
     * Cette méthode retourne le nombre d’utilisateurs inscrits sur le site Amizone
     *
     * @return retourne le nombre d’utilisateurs inscrits sur le site Amizone
     */
    public int getNbrUtilisateurs() {
        return utilisateurs.size();
    }

    /**
     * cette methode verifie si le fournisseur vend au moin un produit avec quantite > 0
     *
     * @param utilisateur le fournisseur à verifier
     * @return true si le fournisseur vend au moin un produit avec quantite > 0,false sinon
     */
    private Boolean hasProduit(Utilisateur utilisateur) {
        Fournisseur fournisseur = (Fournisseur) utilisateur;
        for (Produit produit : fournisseur.getProduits()) {
            if (produit.getQuantite() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * cette methode verifie si le nouveau utilisateur existe dans la liste des utilisateurs de Amizone
     *
     * @param nvUtilisateur l'utilisateur a verifier
     * @return true si l'utilisateur existe dans la liste des utilisateurs de Amizone,false sinon
     */
    private Boolean existDeja(Utilisateur nvUtilisateur) {
        for (Utilisateur utilisateur : utilisateurs) {
            if (utilisateur.equals(nvUtilisateur)) {
                return true;
            }
        }
        return false;
    }

    /**
     * cette methode verifie si l'utilisateure passé en parmetre est de type fournisseur
     *
     * @param utilisateur l'utilisateur a verifier
     * @return true si l'utilisateure passé en parmetre est de type fournisseur,false sinon
     */
    private Boolean isFournisseur(Utilisateur utilisateur) {
        return utilisateur instanceof Fournisseur;
    }

    /**
     * Cette méthode ajoute l’utilisateur donné à la liste des utilisateurs d’Amizone
     *
     * @param utilisateur l'utilisateur a ajouter à la liste des utilisateurs d’Amizone
     * @throws Exception si l’utilisateur donné est null
     *                   ou si l’utilisateur donné est
     *                   un fournisseur (type Fournisseur) et qu’il ne vend aucun produit dont la quantité est strictement plus
     *                   grande que 0.
     *                   ou si l’utilisateur donné est déjà
     *                   dans la liste des utilisateurs d’Amizone.
     */
    public void inscrireUtilisateur(Utilisateur utilisateur)
        throws Exception {

        if (utilisateur == null) {
            throw new Exception(MSG_ERR_UTILIS_NULL);
        } else if (isFournisseur(utilisateur) && !hasProduit(utilisateur)) {
            throw new Exception(MSG_ERR_FOURN_AUCUN_PRODUIT);
        } else if (existDeja(utilisateur)) {
            throw new Exception(MSG_ERR_UTILIS_EXISTANT);
        } else {
            this.utilisateurs.add(utilisateur);
        }
    }


    /**
     * Cette méthode permet de recommander à un consommateur
     * des fournisseurs potentiellement intéressants pour ce consommateur ou bien de recommander à un fournisseur
     * des consommateurs potentiellement intéressés par les produits que vend ce fournisseur
     *
     * @param utilisateur l'utilisateur dont on va lui recommander d'autres utilisateurs
     * @return retourne une liste d’utilisateurs potentiellement intéressants pour l’utilisateur donné
     * @throws Exception si l’utilisateur donné est null.
     */
    public ArrayList<Utilisateur> recommanderUtilisateurs
    (Utilisateur utilisateur) throws Exception {

        ArrayList<Utilisateur> utilisateursRecommande = new ArrayList<>();

        if (utilisateur == null) {
            throw new Exception(MSG_ERR_UTILIS_NULL);
        } else if (isFournisseur(utilisateur)) {
            for (Utilisateur user : utilisateurs) {
                if (!isFournisseur(user) && intersection(utilisateur.compilerProfil(), user.compilerProfil())) {
                    utilisateursRecommande.add(user);
                }
            }
        } else {
            for (Utilisateur user : utilisateurs) {
                if (isFournisseur(user) && intersection(utilisateur.compilerProfil(), user.compilerProfil())) {
                    utilisateursRecommande.add(user);
                }
            }
        }

        return utilisateursRecommande;
    }


    /**
     * cette methode tronsforme la liste donné en majuscules
     *
     * @param arrayList la liste à transormer
     * @return la liste donné en majuscules
     */
    private ArrayList<String> upperCaseList(ArrayList<String> arrayList) {
        ArrayList<String> list = new ArrayList<>();
        for (String element : arrayList) {
            list.add(element.toUpperCase());
        }
        return list;
    }

    /**
     * Cette methode verifie l'existance des elements communs entre 2 listes données
     *
     * @param arrayList1 la 1ere liste a verifier
     * @param arrayList2 la 2ele liste a verifier
     * @return true s'il existe au moin un element commun enre 2 listes données,false sinon
     */
    private boolean intersection(ArrayList<String> arrayList1, ArrayList<String> arrayList2) {
        for (String element1 : upperCaseList(arrayList1)) {
            for (String element2 : upperCaseList(arrayList2)) {
                if (element1.equals(element2)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Cette methode retourne une liste de tous les produits
     * vendus par le fournisseur donné qui sont potentiellement intéressants pour le consommateur donné
     *
     * @param fournisseur  fournisseur donné
     * @param consommateur consommateur donné
     * @return liste de tous les produits
     * vendus par le fournisseur donné qui sont potentiellement intéressants pour le consommateur donné
     * @throws Exception si le fournisseur ou le consommateur donné est null
     */
    public ArrayList<Produit> recommanderProduits
    (Fournisseur fournisseur, Consommateur consommateur)
        throws Exception {

        ArrayList<Produit> produits = new ArrayList<>();

        if (fournisseur == null || consommateur == null) {
            throw new Exception(MSG_ERR_UTILIS_NULL);
        }

        for (Produit produit : fournisseur.getProduits()) {
            if (upperCaseList(consommateur.compilerProfil()).contains(produit.getCategorie().toUpperCase())
                && produit.getQuantite() > 0) {
                produits.add(produit);
            }
        }
        return produits;
    }

    /**
     * Cette méthode permet d’effectuer une transaction achat/vente entre un fournisseur et un consommateur. Elle
     * permet au fournisseur donné de vendre le produit du code donné, de la quantité donnée, au consommateur
     * donné (autrement dit, permet au consommateur donné d’acheter le produit du code donné, de la quantité
     * donnée, et du fournisseur donné)
     *
     * @param fournisseur  fournisseur donné
     * @param consommateur consommateur donné
     * @param codeProduit  code de produit
     * @param quantite     quantité donnée de produit
     * @throws Exception si le code du produit donné ne correspond à aucun des produits vendus par le
     *                   fournisseur donné.
     *                   ou si la quantité donnée est plus
     *                   petite ou égale à 0 ou si elle est plus grande que la quantité en stock du produit ayant le code donné,
     *                   chez le fournisseur donné
     */
    public void effectuerTransaction(Fournisseur fournisseur,
                                     Consommateur consommateur, int codeProduit, int quantite)
        throws Exception {

        int qteStock = 0;
        Produit copieProduit = null;
        Produit produit = fournisseur.obtenirProduit(codeProduit);
        if (produit != null) {
            qteStock = produit.getQuantite();
            copieProduit = new Produit(produit);
            copieProduit.setQuantite(qteStock);
        }
        fournisseur.vendre(codeProduit, quantite);
        consommateur.acheter(copieProduit, quantite);
    }

    /**
     * Cette méthode retourne une liste de tous les produits (parmi les produits vendus par tous les fournisseurs) dont
     * la description contient le mot clé donné les produits retournés doivent avoir une quantité en stock strictement plus grande que 0.
     *
     * @param motCle mot clé donné pour la recherche
     * @return liste de tous les produits (parmi les produits vendus par tous les fournisseurs) dont
     * la description contient le mot clé donné  les produits retournés doivent avoir une quantité en stock strictement plus grande que 0.
     */
    public ArrayList<Produit> rechercherProduitsParMotCle(String motCle) {
        ArrayList<Produit> produits = new ArrayList<>();

        if (motCle == null) {
            return produits;
        }
        for (Utilisateur utilisateur : utilisateurs) {
            if (isFournisseur(utilisateur)) {
                ArrayList<Produit> produitsFournisseur = ((Fournisseur) utilisateur).getProduits();
                for (Produit produit : produitsFournisseur) {
                    if ((produit.getQuantite() > 0) && produit.getDescription().toUpperCase().contains(motCle.toUpperCase())) {
                        produits.add(produit);
                    }
                }
            }
        }
        return produits;
    }

    /**
     * Cette méthode retourne une liste des fournisseurs qui vendent le produit du code donné, et dont la quantité (en
     * stock) est strictement plus grande que 0. De plus, les fournisseurs retournés doivent être ordonnés selon leur
     * évaluation (en ordre décroissant des évaluations ) . Les fournisseurs qui
     * vendent le produit, mais qui n’ont reçu aucune évaluation ne doivent pas être considérés (ils ne doivent pas se
     * trouver dans la liste retournée)
     *
     * @param codeProduit code de produit donné
     * @return retourne une liste des fournisseurs qui vendent le produit du code donné, et dont la quantité (en
     * stock) est strictement plus grande que 0. De plus, les fournisseurs retournés doivent être ordonnés selon leur
     * évaluation (en ordre décroissant des évaluations ) . Les fournisseurs qui
     * vendent le produit, mais qui n’ont reçu aucune évaluation ne doivent pas être considérés (ils ne doivent pas se
     * trouver dans la liste retournée)
     */
    public ArrayList<Fournisseur> rechercherFournisseursParEvaluation(int codeProduit) {

        ArrayList<Fournisseur> fournisseurs = new ArrayList<Fournisseur>();
        for (Utilisateur utilisateur : utilisateurs) {
            if (isFournisseur(utilisateur) && !utilisateur.getEvaluations().isEmpty()) {
                ArrayList<Produit> produitsFournisseur = ((Fournisseur) utilisateur).getProduits();
                for (Produit produit : produitsFournisseur) {
                    if (produit.getQuantite() > 0 && (produit.getCode() == codeProduit)) {
                        fournisseurs.add((Fournisseur) utilisateur);
                    }
                }
            }
        }

        for (int i = 0; i < fournisseurs.size(); i++) {
            for (int j = i + 1; j < fournisseurs.size(); j++) {
                if (fournisseurs.get(i).evaluationMoyenne() < fournisseurs.get(j).evaluationMoyenne()) {
                    Fournisseur temp = fournisseurs.get(i);
                    fournisseurs.set(i, fournisseurs.get(j));
                    fournisseurs.set(j, temp);
                }
            }
        }
        return fournisseurs;
    }
}

