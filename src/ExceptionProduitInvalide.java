
/**
 * Classe d'Exception levee lorsqu'un produit ne peut pas etre cree.
 * @author melanie lord
 * @version Automne 2020
 */
public class ExceptionProduitInvalide extends RuntimeException {
   
}
